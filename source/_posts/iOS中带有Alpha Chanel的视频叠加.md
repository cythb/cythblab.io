title: "iOS中带有Alpha Chanel的视频叠加"
date: 2014-12-02 13:37:41
tags: iOS
categories: iOS
---

最近一个项目中，需要将一些视频特效（下雨，下雪，烟花）合成到另一个录制好的视频中去。于是一头扎进AVFoundation中寻找实现方法。
很遗憾AVFoundation中没有找到简单的方法来实现我要求的效果。
如果你尝试通过AVMutableComposition来尝试添加一个AVAssetTrack的话，你会发现带有Alpha通道的视频播放的时候背景是黑色的——并不是透明的。于是开始尝试将黑色背景编程透明的。找了很多资料都没有得到将黑色背景变成透明的方法。浪费很多时间之后不得不尝试其他途径来实现这个需求。
<!-- more -->

## 一帧一帧的处理两个视频
另一个思路就是将两个视频的每一帧图像进行叠加，最后将音频信息合成到处理好的视频文件中。
![视频叠加](http://ww3.sinaimg.cn/bmiddle/4cec3b1cgw1eq2w4vflmuj20db04yaaq.jpg)

### 读取Frame
{% gist 46d10cb9e80405429e28 %}

### 转换成图片
{% gist 3eb2cec7213bdb2a0f1b %}
但但但是，我们发现读取出来的图片是不透明的。好吧，我们又陷入死胡同啦。这个时候就是AVAnimator大显身手的时候了。

### 躲在角落里的AVAnimator
[AVAnimator](http://www.modejong.com/AVAnimator/) 这个库能将带有Alpha通道的视频文件转换成一个mvid的格式。其实就是将一帧的图像变成了2张图，一张是RGB的图片，另一张是黑白图（表示Alpha）用来表示如何绘制图片。通过AVAnimator我们可以得到合成之后带有Alpha通道的图片。
{% gist ac5aa131567d198d8845 %}

### 处理图片
接下来就简单了，就是处理图片了。下面就是将两张图片合成成一张。
{% gist cabd0099b1022289cf28 %}

### 写入Frame
将处理好的图片写入到一个新的视频文件中。
{% gist 55b372f81322e2d9e7e6 %}

## 声音的合成
最后就是将声音合成进去。
{% gist 0f548d37e704ece6d5e3 %}

## 总结
Demo等以后再放出来吧，比较项目刚出来。接下来我们来简单的梳理一下过程。

1. 读取视频文件Frame
2. 利用AVMvidFrameDecoder来读取带有Alpha通道视频文件Frame
3. 处理读取的两个Frame
4. 写入新的Frame到新的文件
5. 写入声音到新的文件
