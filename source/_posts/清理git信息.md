title: "清理git信息"
date: 2014-10-21 12:37:42
tags:
---

git信息是根据文件夹里面的 .git文件夹保存相关配置的，删除此文件夹即可去掉git信息：

{% codeblock lang:sh %}
find . -type d -name ".git"|xargs rm -rf

# xargs - build and execute command lines from standard input
{% endcodeblock %}
