title: "iOS输出字体信息"
date: 2015-01-15 13:49:32
tags: iOS
categories: iOS
---

{% codeblock lang:objc %}
for(NSString *familyName in [UIFont familyNames]){
    NSLog(@"Font FamilyName = %@",familyName); //*输出字体族科名字

    for(NSString *fontName in [UIFont fontNamesForFamilyName:familyName]){
        NSLog(@"\t%@",fontName);         //*输出字体族科下字样名字
    }
}
{% endcodeblock %}
