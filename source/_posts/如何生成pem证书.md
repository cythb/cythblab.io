title: "如何生成pem证书"
date: 2015-01-20 13:52:50
tags:
---

1. 登录到 iPhone Developer Connection Portal（http://developer.apple.com/iphone/manage/overview/index.action ）并点击 App IDs

2. 创建一个不使用通配符的 App ID 。通配符 ID 不能用于推送通知服务。例如，  com.itotem.iphone

3. 点击App ID旁的“Configure”，然后按下按钮生产 推送通知许可证。根据“向导” 的步骤生成一个签名并上传，最后下载生成的许可证。
<!-- more -->

4. 通过双击.cer文件将你的 aps_developer_identity.cer 引入Keychain中。

5. 在Mac上启动 Keychain助手，然后在login keychain中选择 Certificates分类。你将看到一个可扩展选项“Apple Development Push Services”

6. 扩展此选项然后右击“Apple Development Push Services” > Export “Apple Development Push Services ID123”。保存为 apns-dev-cert.p12文件。

7. 扩展“Apple Development Push Services” 对“Private Key”做同样操作，保存为 apns-dev-key.p12 文件。

8. 需要通过终端命令将这些文件转换为PEM格式：
    openssl pkcs12 -clcerts -nokeys -out apns-dev-cert.pem -in apns-dev-cert.p12
    openssl pkcs12 -nocerts -out apns-dev-key.pem -in apns-dev-key.p12

9. 如果你想要移除密码，要么在导出/转换时不要设定或者执行：
    openssl rsa -in apns-dev-key.pem -out apns-dev-key-noenc.pem

10. 最后，你需要将键和许可文件合成为apns-dev.pem文件，此文件在连接到APNS时需要使用：
    cat apns-dev-cert.pem apns-dev-key-noenc.pem > apns-dev.pem

