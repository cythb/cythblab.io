title: "iOS代码结构"
date: 2014-12-09 13:44:53
tags: iOS
categories: iOS
---

从学习iOS以来虽然经常思考如何组织代码，但是却很少总结。进来看到[一篇文章](http://www.swifthumb.com/article-308-1.html)，觉得之前觉得朦朦胧胧的地方突然豁朗开朗。所以决定也来写一写iOS代码结构的总结。

## 使用workspace来管理项目

我想大家用workspace比较多的情况是使用Cocoapod吧。通过pod来组织依赖的第三方库，然后我们的项目来链接pod.a文件。我们可以通过workspace来管理项目，和其他文档。

![图：workspace管理项目和文档](http://ww3.sinaimg.cn/bmiddle/4cec3b1cgw1eq2wavj99ej207b0bv3z9.jpg)
<!-- more -->

## 代码结构

```
|--Core
|--General
|    |
|    |--Base
|    |--Category
|    |--CustomUI
|    |--Helper
|    |--Marco
|
|--Model
|--Sections
|    |--Login
|    |    |--CustomUI
|    |    |--Manager
|    |    |--Model
|    |    |--ViewController
|    |
|    |--Setting
|
|--Vendors
|--Resource

```

1. Core：工程内一些通用的机制实现类：统一的任务管理，模块管理，服务管理。
2. General：公用类和方法，包括工程内ViewController,UITableViewCell基类(Base)，公用Category(Category)，公用UI组件(CustomUI)，公用辅助方法(Helper)和宏定义(Marco)。
3. Model：公用数据模型
4. Sections：不同程序单元。如登录，设置等等。其下又可以按照功能分成不同的子目录：当前单元使用的自定义UI组件，管理类，数据模型和ViewController等等。
5. Vendors：自己修改或者不支持CocoaPod的第三方库。
6. 资源文件：图片，声音，粒子。其中图片可以通过xcassets来管理。
