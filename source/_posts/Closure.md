title: "Swift 闭包（Closure)"
date: 2014-12-10 13:48:33
tags:
---

最近想开始折腾Swift了。所以整理整理学习笔记。不得不说Swift确实比Objective-C简洁啊。在OC中Closure已经用的比较多了，每次声明的时候都是长长一串。现在终于不用那么麻烦了。不废话了，先来看看Closure的语法吧。

## Closure表达式语法

```swift
{ (parameters) -> return type in
    statements
}
```

简单吧。由于Swift编译器可以推断出变量的类型，所以我们有更简单的写法。
<!-- more -->

## 推断类型

```swift
reversed = sorted(names, { s1, s2 in return s1 > s2 } )
```
由于编译器能推断s1, s2, (s1>s2)的类型，所以我们可以不写。闭包的使用又得到了进一步简化。

## 隐式的返回“单表达式的闭包”
```swift
reversed = sorted(names, { s1, s2 in s1 > s2 } )
```
上面的代码没有使用return，但是由于隐式的返回“单表达式的闭包”的特性，上述代码会返回BOOL值。我们的简化路程还没有完，各位看官继续往下看。

## Shorthand变量名

```swift
reversed = sorted(names, { $0 > $1 } )
```
如果你没有写上形参的名字，我们可以通过$0, $1, $2 ...来逐个访问传入的参数。所以我们又可以省掉参数的声明。

## Trailing Closures
Trailing closure就是调用方法时把实参闭包写到方法括号后面。一般来说在方法的最后一个参数类型是闭包，同时闭包表达式很长，就可以考虑使用这种写法。

```swift
// 这个函数有一个闭包参数，且在最后。
func someFunctionThatTakesAClosure(closure: () -> ()) {
    // function body goes here
}

// 调用这个方法时没有使用Trailing Closures的写法

someFunctionThatTakesAClosure({
    // closure's body goes here
})

// 调用这个方法时使用Trailing Closures的写法

someFunctionThatTakesAClosure() {
    // trailing closure's body goes here
}
```

## 关于Capturing Values
关于值的捕获和OC中比较没有什么变化。

## 其他你需要知道的
1. 全局函数也是闭包，但是它不捕获任何值。即不会对值的生命周期产生影响。
2. 嵌套的方法也是闭包，有一个名字且能捕获它上一层方法中的值。
3. 闭包表达式是没有名字的，会捕获值。

