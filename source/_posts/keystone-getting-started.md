---
title: 入门（keystone文档翻译）
date: 2018-10-30 14:43:52
tags:
- 翻译
- keystone
---

## 什么是Keystone？
<!--Keystone is a powerful Node.js content management system and web app framework built on the Express web framework and Mongoose ODM. Mongoose is an Object-Document Mapper (ODM) which provides a schema-based solution for modelling data and relationships for documents stored in the MongoDB database server. Keystone extends Mongoose's schema-based models with the concept of a Keystone List that helps you build beautiful Admin UIs with intelligent field types.-->
Keystone是一个强大的Node.js内容管理系统，它是建立在Express和Mongoose ODM上的Web App框架。Mongoose ODM是面向文档映射，为那些存储到MongoDB数据库中文档，其提供了面向基于模式的解决方案来为数据和关系建模。Keystone扩展了Mongoose基于模式模型。Keystone列表利用智能字段类型帮助你构建漂亮的Admin界面。

<!--Keystone aims to make it easy for you to create sophisticated web sites and applications, but without restricting how much you can customise your projects. You can bring your own view engine, structure your routes however you want, and modify your data structures to suit your requirements.-->
Keystone的目标是让你更容易的构建复杂网站和应用，而不限制你自定义功能。你可以引入你自己的视图引擎，设计你想要的路由，并且修改你的数据结构来适应你的需求。

## 先决条件
<!--Make sure you have the Node.js JavaScript runtime installed. Keystone 4 has been most throughly tested with Node 6, but should be compatible with newer production releases. We recommend using Node LTS (Long Term Support) releases for their extended support lifecycle and stability.-->
确保你安装好了Node.js JavaScript运行环境。Keystone 4主要是在Node 6上进行测试，但是应当是和最新正式版Node是兼容的。我们推荐使用Node LTS（长期支持）版本，因为更长的支持周期和稳定性。（译者注：LTS 版本更注重稳定性和扩展支持，通常将支持 30 个月。）

<!--You will also need to access to a MongoDB database server either installed locally or hosted remotely. Keystone 4 should be compatible with current production releases of MongoDB.-->
你也要可以访问一个MongoDB数据库，不管是安装在本地还是部署在远程服务器上。Keystone 4兼容最新的MongoDB正式版。

<!--You will need a reasonable working knowledge of JavaScript, Node.js, and npm to use Keystone. Basic familiarity with database concepts and MongoDB would also be helpful.-->
你需要有一定的JavaScript、Node.js和npm的知识来使用Keystone。了解数据库概念和MongoDB则更有帮助。

## 从哪里开始？
### 快速开始指导
<!--If you want to get a project up and running locally with minimal fuss, we recommend starting with the Keystone Yeoman Generator. The Keystone Yeoman Generator provides a prompted experience to help you generate a full project ready to npm start with optional features like a basic blog, feedback form, and image gallery.-->
如果你想要在本地快速运行一个项目，我们推荐你从Keystone Yeoman生成器开始。Keystone Yeoman生成器提供了一个提示命令行来帮助你生成完整的项目，可以通过npm来定制一些特性如基础的博客，反馈表单，相册功能。

### 从脚手架开始设置
<!--If you prefer to write your code from scratch, check out our four part Setting Up guide. This guide will walk you through setting up the core files and settings to get you started with Keystone.-->
如果你更喜欢从脚手架编写你的代码，产看我们第四部分的设置教程。这个教程让你从核心的文件和设置来开始Keystone。

## 接下来去哪？
<!--Check out our documentation if you want to learn more about Keystone and the available options. Database Configuration is a good place to start, as are the Keystone Setup Options for general configuration.-->
如果你想要学习更多的Keystone设置选项，请查阅我们的文档。数据库配置是一个好的起点，同样Keystone通用设置选项也是不错的选择。

