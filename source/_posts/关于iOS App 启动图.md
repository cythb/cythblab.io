title: iOS App 启动图
date: 2015-04-20 16:30:33
tags: iOS
categories: iOS
---

iOS App在打开的时候一般会显示一张启动图片。这样用户会觉得程序打开速度和响应速度很快。***启动图片只有一个用途就是让用户觉得程序响应很快，除此之外没有其他作用。***

## 启动图片的内容

启动图片能帮助我们提高App的用户体验，接下来我们看看启动图片应该是什么样子的。
在启动图片中，我们不应该用来提供这些内容：
1. 一些进入的效果，如一个被溅射的屏幕
2. 关于窗口
3. 一些打上烙印的元素。（除非这些元素出现在即将出现的第一个程序界面。）

<!-- more -->

启动图片应该和程序的第一个界面一致，除了：
1. 文字。 启动图片是静态的， 因此这些文字不会被国际化。
2. 会改变的UI元素。 如果启动界面包含的UI元素和启动后的第一个程序界面中UI元素不一样，会引起闪烁，这会让用户对你App留下不好的印象。

下面是手机中自带程序的启动图：

1. 设置程序的启动图
![设置程序的启动图](http://7xihd8.com1.z0.glb.clouddn.com/settings_launch_2x.png)
2. 天气程序的启动图
![天气程序的启动图](http://7xihd8.com1.z0.glb.clouddn.com/weather_launch_2x.png)

## 如何在项目中设置启动图片

在iOS 8和之后的版本中，苹果推荐使用xib或者storyboard 文件来提供启动时的图片。
对于iPhone 6及之后的设备，苹果推荐使用xib或者storyboard 文件来提供启动时的图片。
其他情况使用静态的图片作为启动图片。


如果你使用的是提供静态图片的方式来作为启动图片的话，那么你就不能使用autolayout来进行布局。下面的表格中会列出每种设备需要启动图的尺寸。

|iPhone 6 Plus (@3x)|iPhone 6 and <br/>iPhone 5 (@2x)|iPhone 4s (@2x)|iPad and iPad mini (@2x)|iPad 2 and iPad mini (@1x)|
|:------|:------|:--------|:--------|:--------|
| Use a launch file (see Launch Images)  | For iPhone 6, use a launch file (see Launch Images) <br/>For iPhone 5, 640 x 1136 | 640 x 960  | 1536 x 2048 (portrait) <br/>2048 x 1536 (landscape) | 768 x 1024 (portrait) <br/>1024 x 768 (landscape) |

当静态图片按照上述尺寸设计好之后，接下来我们推荐这种方式来将静态图片设置成启动图片。
1. 使用Xcode打开项目工程。
2. 选中Target，在General选项下找到Launch Images Source点击Use Asset Catalog
![Use Asset Catalog](http://7xihd8.com1.z0.glb.clouddn.com/use_asset_catalog_for_launch.png)
3. 找到Images.xcassets文件，将静态图片分别拖入到对应的位置。
