title: "也谈谈MVVM（一）"
date: 2015-08-12 18:55:33
tags: 开发模式
---

从学习iOS开发就开始接触到MVC的开发模式了。随着越来越注重用户体验以及业务越来越复杂。ViewController也越来越臃肿，同时我们的工作量也越来越大。在ViewController中混杂着业务逻辑与UI处理的逻辑。完整的测试ViewController既要对UI进行测试也需要对业务逻辑进行测试。测试起来比较麻烦。

>物极必反 器满则倾

接下来进入正题，看看MVVM设计模式是如何解决这些问题的。

*MVVM = Model + View + ViewModel*

看上去和MVC差不多，就是把ViewController替换成了ViewModel。如果是这样的话，MVVM压根就没有存在的必要了。所以并不是进行了简单的替换。MVVM由3部分组成：Model ， View ， ViewModel。

* Model: 作为数据的容器
* View: 负责界面的展示以及用户交互的处理
* ViewModel: 负责业务逻辑处理

##iOS中的MVVM

然并卵iOS中我们没有办法绕过UIViewController。诸如UITabController， UINavigationController是非常非常常用的。没有这些容器，我们写界面时的情景简直不敢想象。IB与UIViewController的结合比较紧密。如果你通过IB来画界面的话，更离不开UIViewController。那么iOS中的MVVM是什么样的?

![图:捂脸](http://7xihd8.com1.z0.glb.clouddn.com/捂脸.jpg)

* Model: 作为数据的容器
* View: 负责界面的展示以及用户交互的处理
* **ViewController: 用代码创建视图；胶水代码，连接View和ViewModel**
* ViewModel: 负责业务逻辑处理

Model，View，ViewModel的职责都没有改变。iOS的MVVM开发模式中ViewController只做与视图有关的操作，以及连接View与ViewModel的胶水代码。也就是说View部分其实是由*原本的View＋部分ViewController*组成。

一旦我们这样组织代码，首先进行业务逻辑测试的时候我们只需要测试ViewModel就可以。测试UI只需要测试View＋ViewController。对于UI的自动化测试我一直没有找到什么特别好的办法，可能主要靠手工和一些第三方的测试平台吧。其次我们在开发过程中可以进行并行开发了，即可以同时开发界面和业务逻辑。即使你是一个人开发App，那么每一次只做一件事情同样会让你的头脑更清楚。

![图:开心](http://img2.duitang.com/uploads/item/201301/24/20130124215409_kumv5.thumb.600_0.jpeg)

##胶水代码

>iOS中没有胶水代码的MVVM是不完整的

胶水代码都做了哪些事情？

1. 将控件的事件传递给ViewModel，使得ViewModel其有机会处理用户交互。
2. 将ViewModel的内容与View进行绑定，使得View有机会显示正确的内容。

在ReactiveCocoa出来之前，胶水代码很难写的很优雅。这也是为什么以前iOS中MVVM并不怎么火的原因。关于ReactiveCocoa的学习与使用本文不会涉及。如果有需要可以自行搜索学习。它是响应式开发的利器。

>iOS的MVVM开发模式不能没有"ReactiveCocoa"

没有胶水代码，你不能将业务逻辑从ViewController中抽离。

也谈谈MVVM（二）将会进行实战演练。结合一个简单的Demo进行讲解MVVM开发模式。敬请期待！另外欢迎大家能和我一起讨论交流，一起进步。

未完待续...

![图:三只企鹅](http://ww1.sinaimg.cn/mw1024/a44e4a7ejw1e7xe35l8c4j20h80bhq4f.jpg)
