title: "生成iOS模拟器Build"
date: 2014-11-12 12:52:10
tags: iOS
categories: iOS
---

# 准备工作：
1. 安装 XCODE
2. Xcode版本小于6, 需要安装ios-sim

# 安装环境
如果上诉两个条件有一个没有完成，就请继续往下看。否则跳到开始部分。

## 安装 XCODE
请去AppStore[下载并安装](https://itunes.apple.com/cn/app/xcode/id497799835?mt=12)。
<!-- more -->

## 安装 ios-sim
[ios-sim](https://github.com/phonegap/ios-sim)是一个开源的项目。如果你感兴趣可以点击链接进去看看。

安装ios-sim有三种方法。

1. 如果你安装了node.js（0.10.20以上）。使用下面命令进行安装。
{% codeblock %}
$ sudo npm install ios-sim -g
{% endcodeblock %}

2. 下载编译好的进行安装。
{% codeblock %}
$ curl -L https://github.com/phonegap/ios-sim/zipball/3.0.0 -o ios-sim-3.0.0.zip
$ unzip ios-sim-3.0.0.zip
{% endcodeblock %}

3. 下载开发版本进行安装。
{% codeblock %}
$ git clone git://github.com/phonegap/ios-sim.git
$ rake install prefix=/usr/local/
{% endcodeblock %}

# 正式开始
下面进入正题。

## 生成app文件

命令行下导航到项目目录.


{% codeblock %}
xcodebuild -arch i386 -sdk iphonesimulator8.1 -workspace YOURWORKSPACENAME.xcworkspace -scheme YOURSCHEMNAME -configuration Release
{% endcodeblock %}

## 验证模拟器是否能安装

通过下面命令将app安装到模拟器上面。

### Xcode版本是6以下

{% codeblock %}
ios-sim launch [path to .app]
{% endcodeblock %}

### Xcode版本是6或以上

Xcode 6引入了xcrun simctl可以控制模拟器。首先我们来安装app到模拟器。

{% codeblock %}
xcrun simctl install booted [Your app path]
{% endcodeblock %}

然后启动app

{% codeblock %}
xcrun simctl launch booted [Your app bundle id]
{% endcodeblock %}

# 总结

1. 通过xcodebuild命令生成app文件
2. 通过ios-sim验证app文件是否正常
