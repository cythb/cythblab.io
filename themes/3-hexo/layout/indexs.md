到目前为止已经写了<code class="article_number"></code>篇文章， 共<code class="site_word_count"></code>字。

本站访问人数：<code class="site_uv"></code>人次 ， 访问量：<code class="site_pv"></code>次

主要功能：
- 搜索支持文章标题、标签(#标签)、作者(@作者)
- pad/手机等移动端适配
- 页面全局快捷键 <a href='http://yelog.org/2017/03/24/3-hexo-shortcuts/'>3-hexo快捷键说明</a>
