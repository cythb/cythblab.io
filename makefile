.SILENT:
.PHONY: test deploy

test:
	./hexo clean && ./hexo g
	cp -r copy-to-img/ ./public/img/
	open "http://localhost:4000"
	./hexo s

deploy:
	git push origin master
